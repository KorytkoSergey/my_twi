# Generated by Django 4.2.1 on 2023-05-04 09:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kombinaciay', '0003_student_date_birth_student_dc_student_dm'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
            ],
        ),
        migrations.CreateModel(
            name='University',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
            ],
        ),
        migrations.CreateModel(
            name='StudentId',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numbers', models.CharField(max_length=252, unique=True)),
                ('student', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='studentid', to='kombinaciay.student')),
            ],
        ),
        migrations.AddField(
            model_name='student',
            name='lesson',
            field=models.ManyToManyField(blank=True, to='kombinaciay.lesson'),
        ),
        migrations.AddField(
            model_name='student',
            name='university',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='student', to='kombinaciay.university'),
        ),
    ]
