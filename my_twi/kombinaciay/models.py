import uuid

from django.db import models

# Create your models here.
GENDER_CHOICE = (
    ('M', 'man'),
    ('F', 'woman')
)

class University(models.Model):
    name = models.CharField('Название', max_length=255)
    def __str__(self):
        return self.name

class Lesson(models.Model):
    name = models.CharField('Название', max_length=255)

class Student(models.Model):
    first_name = models.CharField('Имя', max_length=255)
    second_name = models.CharField('Фамилия', max_length=255)
    gender = models.CharField('Пол', max_length=255, choices=GENDER_CHOICE,
                              default=GENDER_CHOICE[0])
    lesson_count = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    date_birth = models.DateField(blank=True, null=True)
    dc = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    dm = models.DateTimeField(auto_now=True, blank=True, null=True)
    university = models.ForeignKey(University,
                                   on_delete=models.CASCADE,
                                   related_name='student',
                                   null=True,
                                   blank=True)
    lesson = models.ManyToManyField(Lesson, blank=True)


class StudentId(models.Model):
    number = models.CharField(max_length=252, unique=True)
    student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name='studentid')

    class Meta:
        verbose_name = 'Студент'
        verbose_name_plural = 'Студенты'
        ordering = ('number',)
        abstract = True

    def __str__(self):
        return f'{self.number} - {self.student.name}'
