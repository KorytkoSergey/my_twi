from django.apps import AppConfig


class KombinaciayConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kombinaciay'
