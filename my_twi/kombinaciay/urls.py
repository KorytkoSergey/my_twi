from django.urls import path, include
from kombinaciay import views

app_name = 'kombinaciay'
urlpatterns = [
    path('index/', views.index, name='index'),
    path('student_list/', views.StudentList.as_view(), name='students_list'),
    path('student_detail/<int:pk>', views.StudentDetail.as_view(), name='student_detail'),
    path('student_create/', views.StudentCreate.as_view(), name='student_create'),
    path('student_update/<int:pk>', views.StudentUpdate.as_view(), name='student_update'),
    path('student_del/<int:pk>', views.StudentDelete.as_view(), name='student_del'),
]