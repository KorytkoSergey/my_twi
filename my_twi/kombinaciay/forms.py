from django import forms

from kombinaciay import models


class StudentSearch(forms.Form):
    first_name = forms.CharField(label='Поиск по имени', required=False, help_text='Поиск по имени студента')
    lesson_count = forms.IntegerField(label='Количество занятий', min_value= 0, required=False)

    def clean_first_name(self):
        name = self.cleaned_data['first_name']
        if '/' in name:
            raise forms.ValidationError('Имя не должно содержать /')
        return name

    def clean(self):  # тоже самое что и функция выше, но можно сразу несколько условий записать
        count = self.cleaned_data['lesson_count']
        if count == 10:
            raise forms.ValidationError('Число не равно 10')
        return count

class StudentCreate(forms.ModelForm):
    first_name = forms.CharField(label='Имя', required=False)
    lesson_count = forms.IntegerField(label='Количество занятий', min_value=0, required=False)

    def clean_first_name(self):
        name = self.cleaned_data['first_name']
        if name.isdigit():
            raise forms.ValidationError('Имя не должно быть числом !')
        return name


    class Meta:
        model = models.Student
        fields = '__all__'
