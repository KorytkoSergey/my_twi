from django.contrib import admin
from kombinaciay import models
@admin.register(models.Student)
class Student(admin.ModelAdmin):
    list_display = ('first_name',)