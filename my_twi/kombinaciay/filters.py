import django_filters as filters
from kombinaciay import models
class StudentFilter(filters):
    class Meta:
        models = models.Student
        fields = ['first_name',]