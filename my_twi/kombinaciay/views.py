from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from kombinaciay import models

from kombinaciay import forms

from kombinaciay import filters


class TitleMixin:
    title = None

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['title']= self.get_title()
        return context

def index(request):
    user = request.user.username
    html = "<html> <body> <h1> Hello %s</h1> </body> </html>" % user
    return HttpResponse(html)

class StudentList(TitleMixin, ListView):
    model= models.Student
    template_name = 'kombinaciay/student_list'
    context_object_name = 'students'
    title = 'Список студентов'

    def get_filters(self):
        return filters.StudentFilter(self.request.GET)
    def get_queryset(self):
        # first_name = self.request.GET.get('first_name')
        # lesson_count = self.request.GET.get('lesson_count')
        # qs = models.Student.objects.all()
        # if first_name:
        #     return qs.filter(Q(first_name__icontains=first_name) | Q(lesson_count=lesson_count))
        return self.get_filters().qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = forms.StudentSearch(self.request.GET or None)
        context['filter'] = self.get_filters()
        context['count'] = self.get_queryset().count()
        return context


class StudentDetail(TitleMixin, DetailView):
    model = models.Student
    template_name = 'kombinaciay/student_detail.html'
    context_object_name = 'student'
    title = 'Студент'


class StudentCreate(TitleMixin, CreateView):
    model = models.Student
    template_name = 'kombinaciay/student_create.html'
    form_class = forms.StudentCreate
    success_url = reverse_lazy('kombinaciay:students_list')

class StudentUpdate(TitleMixin, UpdateView):
    model = models.Student
    template_name = 'kombinaciay/student_create.html'
    fields = ('lesson_count',)
    success_url = reverse_lazy('kombinaciay:students_list')

class StudentDelete(TitleMixin, DeleteView):
    model = models.Student
    template_name = 'kombinaciay/student_del.html'
    success_url = reverse_lazy('kombinaciay:students_list')